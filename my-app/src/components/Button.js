function Button(props) {
  function targetEvent(Event) {
    console.log(Event.target);
  }
  return <button onClick={targetEvent}>{props.label}</button>;
}

export default Button;
